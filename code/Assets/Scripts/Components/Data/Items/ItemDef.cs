﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

[Serializable]
public struct ItemDef {
    public int Quantity;
    public string Path;

    public ItemDef(string path, int quantity) {
        Path = path;
        Quantity = quantity;
    }
}