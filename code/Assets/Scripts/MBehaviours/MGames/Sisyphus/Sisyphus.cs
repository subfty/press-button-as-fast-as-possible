﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Sisyphus : FightPlaceholder {
    #region Game Object bindings        
    [SerializeField]
    private GameObject _DotContainer;
    #endregion

    #region Properties
    [SerializeField]
    private DiffRangeProperty _Gravity = new DiffRangeProperty(90, 50); 
    [SerializeField]
    private DiffRangeProperty _JumpValue = new DiffRangeProperty(50, -15); 
    #endregion

    FightDot _Dot;
    RectTransform _TDot;
    float _FallVelocity = 0.0f;

    protected override void Start() {
        base.Start();

        // initialisation
        if (_Dot == null) {

            _Dot = SpawnDot();
            _Dot.transform.SetParent(_DotContainer.transform, false);
            _TDot = _Dot.GetComponent<RectTransform>();
        }

        if (_GemTypes != null)
            _Dot.SetGemTypes(_GemTypes);

        _FallVelocity = 0.0f;

        _TDot.anchoredPosition = new Vector2(50, 0);
    }    

    protected override void GameUpdate() {
        base.GameUpdate();

        float delta = Time.deltaTime;

        _FallVelocity += delta * _Gravity.GetValue(GameDifficulty);

        if (JustPressed)
            _FallVelocity -= _JumpValue.GetValue(GameDifficulty);
        
        _FallVelocity = Math.Max(
            -_JumpValue.GetValue(GameDifficulty), 
            _FallVelocity);        
            
        _TDot.anchoredPosition = new Vector2(Math.Max(0, _TDot.anchoredPosition.x + _FallVelocity * delta), 0);

        float rotVal = _FallVelocity * delta;
        if (_TDot.anchoredPosition.x <= 0.0f)
            rotVal = 0.0f;

        _TDot.localRotation = 
            Quaternion.Euler(new Vector3(
                _TDot.localRotation.eulerAngles.x,
                _TDot.localRotation.eulerAngles.y,
                _TDot.localRotation.eulerAngles.z - rotVal ));

        if (_TDot.anchoredPosition.x >= 360)
            GameFailed();
    }
}
