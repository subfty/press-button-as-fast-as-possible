﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyMessenger;
using UnityEngine;
using UnityEngine.UI;

public class FightPlaceholder : EMonoBehaviour {

    public const float PLAY_AREA_WIDTH = 440.0f;
    public const float PLAY_AREA_HEIGHT = 600.0f;

    [Header("Game Parameters")]
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float _GameDifficulty = 0;
    [SerializeField]
    private float _GameDuration = 6;

    protected float _TimeLeft;
    protected List<FightConfig.GemType> _GemTypes;

    #region Properties
    public float GameDifficulty {
        get { return _GameDifficulty; }
    }

    public float GameProgress {
        get { return Mathf.Clamp01(_TimeLeft / _GameDifficulty); }
    }

    public float GameDuration {
        get { return _GameDuration; }
    }

    // Input wrapper
    public bool JustPressed {
        get {
            if (EContainer != null)
                return EContainer.Entity.hasInputReceiver && EContainer.Entity.inputReceiver.JustPressed;

            // debug input handler
            return Input.GetKeyDown(KeyCode.Space);
        }
    }

    public bool JustReleased {
        get {
            if (EContainer != null)
                return EContainer.Entity.hasInputReceiver && EContainer.Entity.inputReceiver.JustReleased;

            // debug input handler
            return Input.GetKeyUp(KeyCode.Space);
        }
    }

    public bool IsPressed {
        get {
            if (EContainer != null)
                return EContainer.Entity.hasInputReceiver && EContainer.Entity.inputReceiver.TouchHold > 0.0f;

            // debug input handler
            return Input.GetKey(KeyCode.Space);
        }
    }
    #endregion

    protected virtual void Start() {

        _TimeLeft = 0.0f;
        TinyMessengerHub
            .Instance
            .Publish<Msg.FightGameProgress>(Msg.FightGameProgress.Get(_TimeLeft / _GameDuration));
    }

    protected override void OnDestroy() {
        base.OnDestroy();
    }

    protected void Update() {
        if (EContainer != null && !EContainer.Entity.hasInputReceiver) return;

        _TimeLeft += Time.deltaTime;

        GameUpdate();

        TinyMessengerHub.Instance.Publish<Msg.FightGameProgress>(
            Msg.FightGameProgress.Get(_TimeLeft / _GameDuration));

        if (_TimeLeft >= _GameDuration)
            GamePassed();
    }

    protected virtual void GameUpdate() {
    }

    protected FightDot SpawnDot() {
        FightDot dot = AddGameObjectSystem.InstantiatePrefab("ui/fight_dot").GetComponent<FightDot>();
        //TODO: actually get info  about this colours from somewhere
        dot.SetGemTypes(new List<FightConfig.GemType>(new FightConfig.GemType[] { 
            FightConfig.GemType.REGULAR_0, 
            FightConfig.GemType.REGULAR_1 }));
        return dot;
    }

    protected void GamePassed() {
        if (EContainer != null) {
            if (!EContainer.Entity.hasInputReceiver) return;

            EContainer
                .Entity
                .RemoveInputReceiver();
            TinyMessengerHub
                .Instance
                .Publish<Msg.FightMiniGameFinished>(Msg.FightMiniGameFinished.Get(1.0f));
        } else {
            // debug input handler
            Debug.LogError("GAME SUCCEDED!");
            OnDestroy();
            Start();
        }
    }

    protected void GameFailed() {
        Debug.LogError("GAME FAILED!");
        OnDestroy();
        Start();
    }

    #region Object Pool Utils
    protected T GetRecycled<T>(List<T> pool, Func<T> factory) where T : Component {
        T newObj = null;
        for (int i = 0; i < pool.Count(); i++)
            if (!pool[i].gameObject.activeSelf) {
                newObj = pool[i];
                break;
            }

        if (newObj == null) {
            newObj = factory();
            pool.Add(newObj);
        }

        newObj.gameObject.SetActive(true);
        return newObj;
    }

    protected void Recycle<T>(T o) where T : Component {
        DOTween.Kill(o, false);
        o.gameObject.SetActive(false);
    }

    protected void RecyclePool<T>(List<T> pool) where T : Component {
        foreach (T o in pool) {
            DOTween.Kill(o, false);
            o.gameObject.SetActive(false);
        }
    }
    #endregion
}
