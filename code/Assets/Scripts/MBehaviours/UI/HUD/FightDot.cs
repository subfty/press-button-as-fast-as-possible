﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class FightDot : MonoBehaviour {
    [SerializeField]
    protected int _DotRadius = 40;
    [SerializeField]
    protected Sprite _DotImage;

    public void SetGemTypes(List<FightConfig.GemType> types) {
        gameObject.RemoveAllChildren();

        for (int i = 0; i < types.Count; i++) {
            GameObject obj = new GameObject();
            Image image = obj.AddComponent<Image>();
            image.transform.SetParent(transform, false);

            image.sprite = _DotImage;
            image.type = Image.Type.Filled;
            image.fillMethod = Image.FillMethod.Radial360;
            image.fillAmount = 1.0f / types.Count;            
            image.color = Color.blue;
            image.raycastTarget = false;
            RectTransform t = image.GetComponent<RectTransform>();

            t.rotation = Quaternion.Euler(
                t.rotation.eulerAngles.x,
                t.rotation.eulerAngles.y, 
                i * (1.0f / types.Count) * 360.0f);
            t.sizeDelta = new Vector2(_DotRadius, _DotRadius);
            t.anchoredPosition = new Vector2(0, 0);
        }
    }

    [ContextMenu("Test Gem 1")]
    protected void TestGem1() {
        SetGemTypes(new List<FightConfig.GemType>(
            new FightConfig.GemType[] { FightConfig.GemType.REGULAR_0 }));
    }

    [ContextMenu("Test Gem 2")]
    protected void TestGem2() {
        SetGemTypes(new List<FightConfig.GemType>(
            new FightConfig.GemType[] { FightConfig.GemType.REGULAR_0, FightConfig.GemType.REGULAR_1 }));
    }

    [ContextMenu("Test Gem 3")]
    protected void TestGem3() {
        SetGemTypes(new List<FightConfig.GemType>(
            new FightConfig.GemType[] { FightConfig.GemType.REGULAR_0, FightConfig.GemType.REGULAR_1, FightConfig.GemType.REGULAR_2 }));
    }

    [ContextMenu("Test Gem 4")]
    protected void TestGem4() {
        SetGemTypes(new List<FightConfig.GemType>(
            new FightConfig.GemType[] { FightConfig.GemType.REGULAR_0, FightConfig.GemType.REGULAR_1, FightConfig.GemType.REGULAR_2, FightConfig.GemType.REGULAR_3 }));
    }
}

