﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(SoundController), true)]
public class SoundControllerInspector : Editor {

    public override void OnInspectorGUI() {
        SoundController manager = target as SoundController;

        #region Sources    
        GUILayout.Space(3);
        GUILayout.Label("Audio Sources", EditorStyles.boldLabel);
        GUILayout.Space(3);

        Array sourcesArray = Enum.GetValues(typeof(SoundController.AudioSources));
        List<SoundController.AudioSources> sourcesList = new List<SoundController.AudioSources>();

        foreach (SoundController.AudioSources t in sourcesArray)
            sourcesList.Add(t);

        while (manager._Sources.Count() < sourcesList.Count())
            manager._Sources.Add(null);        
        while (manager._Sources.Count() > sourcesList.Count())
            manager._Sources.RemoveAt(manager._Sources.Count() - 1);        

        for (int i = 0; i < manager._Sources.Count; i++) {
            manager._Sources[i] =
                (AudioSource)EditorGUILayout.ObjectField(
                    sourcesList[i].ToString(),
                    manager._Sources[i],
                    typeof(AudioSource),
                    null);            
        }
        #endregion

        #region Sounds
        GUILayout.Space(3);
        GUILayout.Label("Sounds", EditorStyles.boldLabel);
        GUILayout.Space(3);

        Array templateArray = Enum.GetValues(typeof(SoundController.Sounds));
        List<SoundController.Sounds> templateList = new List<SoundController.Sounds>();

        foreach (SoundController.Sounds t in templateArray)
            templateList.Add(t);

        while (manager._AudioClips.Count() < templateList.Count())
            manager._AudioClips.Add(null);
        while (manager._AudioClips.Count() > templateList.Count())
            manager._AudioClips.RemoveAt(manager._AudioClips.Count() - 1);

        while (manager._SourcesBaseVolumes.Count() < templateList.Count())
            manager._SourcesBaseVolumes.Add(1.0f);
        while (manager._SourcesBaseVolumes.Count() > templateList.Count())
            manager._SourcesBaseVolumes.RemoveAt(manager._SourcesBaseVolumes.Count() - 1);

        for (int i = 0; i < manager._AudioClips.Count; i++) {
            EditorGUILayout.BeginHorizontal();

            manager._AudioClips[i] =
                (AudioClip)EditorGUILayout.ObjectField(
                    templateList[i].ToString(),
                    manager._AudioClips[i],
                    typeof(AudioClip),
                    null);

            manager._SourcesBaseVolumes[i] =
                EditorGUILayout.Slider(manager._SourcesBaseVolumes[i], 0.0f, 1.0f, GUILayout.MaxWidth(150));
            EditorGUILayout.EndHorizontal();
        }
        #endregion
    }
}